const appKey = "7628fc23319bfdafb90978f2ad6a943f";

let searchButton = document.getElementById("search-btn");
let searchInput = document.getElementById("search-txt");
let temperature = document.getElementById("temp");
let humidity = document.getElementById("humidity-div");
let weather = document.getElementById("weather-div");
let weatherDesc = document.getElementById("weather-div-desc");

searchButton.addEventListener("click", findWeatherDetails);
searchInput.addEventListener("keyup", enterPressed);

function enterPressed(event) {
  if (event.key === "Enter") {
    findWeatherDetails();
  }
}

function findWeatherDetails() {
  if (searchInput.value === "") {

  }else {
    let searchLink = "https://api.openweathermap.org/data/2.5/weather?q=" + searchInput.value + "&appid="+appKey;
    httpRequestAsync(searchLink, theResponse);
  }
}

function theResponse(response) {
  let jsonObject = JSON.parse(response);
  temperature.innerHTML = "Temperature: " + parseInt(jsonObject.main.temp - 273) + " C°";
  humidity.innerHTML = "Humidity: " + jsonObject.main.humidity + "%";
  weather.innerHTML = jsonObject.weather[0].main;
  weatherDesc.innerHTML = jsonObject.weather[0].description;

}

function httpRequestAsync(url, callback)
{
  console.log("Got the data from api.openweathermap.org");
  var httpRequest = new XMLHttpRequest();
  httpRequest.onreadystatechange = () => {
    if (httpRequest.readyState == 4 && httpRequest.status == 200)
    callback(httpRequest.responseText);
  }
  httpRequest.open("GET", url, true); // true for asynchronous
  httpRequest.send();
}
function Home(){
    location.replace("index.html")
}
function ControlPanel(){
    location.replace("loggedin.html")
}