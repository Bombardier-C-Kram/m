firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.

    document.getElementById("user_div").style.display = "block";
    document.getElementById("login_div").style.display = "none";

    var user = firebase.auth().currentUser;
  } else {
    // No user is signed in.

    location.replace("auth.html")

  }
});
function Home(){
  location.replace("index.html")
}
function LoginPage(){
  location.replace("auth.html")
}
function Weather(){
  location.replace("weather.html")
}
